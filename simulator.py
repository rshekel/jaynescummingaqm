import numpy as np
from numpy import sqrt, e, exp, abs, pi
from scipy.linalg import expm
import matplotlib.pyplot as plt
from scipy.special import factorial
from tqdm import tqdm


hbar = 1.0545718e-34
h = 6.62607004e-34


class JaynesCummingSimulation(object):
    N_bstates = 100
    g = 2*pi*30e6
    w = 2*pi*6e9

    def __init__(self):
        self.H = None
        self.U = None
        self.wa = None

    def _init_H_U(self, dt):
        self.H = self.get_H()
        self.U = self.get_U(dt)

    def get_H(self):
        dim = 1 + 2*self.N_bstates
        H = np.zeros((dim, dim))
        H[0, 0] = -0.5*self.wa*hbar

        for n in range(1, self.N_bstates + 1):
            ind = 1 + 2*(n-1)
            H[ind:ind+2, ind:ind+2] = self.get_2_by_2(n)

        return H

    def get_2_by_2(self, n):
        # Basis of
        # | n+1,g |
        # |  n,e  |
        A = np.zeros((2, 2))
        A[0, 0] = (n+1)*self.w - 0.5*self.wa
        A[0, 1] = -self.g*sqrt(n+1)
        A[1, 0] = -self.g*sqrt(n+1)
        A[1, 1] = n*self.w + 0.5*self.wa

        A = A*hbar

        return A

    def get_U(self, dt):
        return expm(-(1j / hbar) * self.H * dt)

    def evolve_dt(self, state_in):
        return self.U @ state_in

    def get_initial_state(self, section_b=False):
        if not section_b:
            v = np.zeros(1 + 2*self.N_bstates)
            alpha = 7
            v_c = self.get_coherent_state(alpha)
            v[0] = v_c[0]  # |0,g> is special.
            v[1::2] = v_c[1:]

            return v

        else:
            v = np.zeros(1 + 2 * self.N_bstates)
            alpha = 7
            v_c = self.get_coherent_state(alpha)
            v[0] = v_c[0]  # |0,g> is special.
            v[1::2] = v_c[1:]
            v[2::2] = v_c[:-1]

            v = v / sqrt(2)

            return v

    def get_coherent_state(self, alpha):
        n = np.arange(self.N_bstates+1)  # type == int32
        C = exp(-abs(alpha) ** 2 / 2)
        # in alpha ** n if keeping as int32 we get overflow, but we do want int for the factorial
        state = C * (alpha ** n.astype(float)) / (sqrt(factorial(n)))
        return state

    def show_occupations(self, A, dt):
        fig, ax = plt.subplots()
        im = ax.imshow(np.abs(A) ** 2, aspect='auto', extent=(0, dt*A.shape[1], A.shape[0], 0))
        fig.colorbar(im, ax=ax)
        ax.set_title("Mode occupation over time")
        ax.set_xlabel('time (s)')
        ax.set_ylabel('combined mode index')
        fig.show()

    def show_power_in_g(self, A, dt):
        fig, ax = plt.subplots()
        Ag = np.abs(A[:1 + self.N_bstates, :])**2
        powers = Ag.sum(axis=0)

        ax.plot(np.arange(len(powers)) * dt, powers)
        ax.set_title("Intensity in ground state")
        ax.set_xlabel('time (s)')
        ax.set_ylabel('Normalized Intensity')
        fig.show()

    def show_purities(self, purities_TLS, purities_bosons, dt):
        fig, ax = plt.subplots()
        ax.plot(np.arange(len(purities_TLS)) * dt, purities_TLS.real)
        ax.set_title("Purity of TLS system")
        ax.set_xlabel('time (s)')
        ax.set_ylabel('Purity of TLS system')
        fig.show()

        fig, ax = plt.subplots()
        ax.plot(np.arange(len(purities_bosons)) * dt, purities_bosons.real)
        ax.set_title("Purity of bosons system")
        ax.set_xlabel('time (s)')
        ax.set_ylabel('Purity of bosons system')
        fig.show()

    def _v_to_better_repr(self, v):
        vv = np.zeros(2*self.N_bstates, dtype=complex)
        vv[0] = v[0]  # g,0
        # gggg - but throw away the g,N+1, because it breaks symmetry. we want a da*db matrix: 2*N
        vv[1:self.N_bstates] = v[1:-2:2]
        vv[self.N_bstates:] = v[2::2]  # eeee

        return vv

    def _mat_to_viewing_order(self, A):
        # instead of the vector being g,g,e,g,e,g,e etc. It is nicer to have it gggggggeeeeeeee
        Ag = A[0, :]
        Ag2 = A[1::2, :]
        Ag = np.row_stack((Ag, Ag2))
        Ae = A[2::2, :]

        Q = np.concatenate((Ag, Ae))
        return Q

    def get_purities(self, v):
        vv = self._v_to_better_repr(v)
        rho = np.outer(vv, vv.conjugate())

        # following https://www.peijun.me/reduced-density-matrix-and-partial-trace.html
        rho_tensor = rho.reshape([2, self.N_bstates, 2, self.N_bstates])
        rho_TLS = rho_tensor.trace(axis1=1, axis2=3)
        rho_bosons = rho_tensor.trace(axis1=0, axis2=2)

        purity_TLS = (rho_TLS @ rho_TLS).trace()
        purity_bosons = (rho_bosons @ rho_bosons).trace()

        return purity_TLS, purity_bosons

    def simulate_revival(self):
        N_steps_a = 8000
        dt = 0.2e-9
        self.wa = 2 * pi * 6e9
        self._init_H_U(dt)

        A = np.zeros((1 + 2*self.N_bstates, N_steps_a), dtype=complex)
        purities_TLS = np.zeros(N_steps_a, dtype=complex)
        purities_bosons = np.zeros(N_steps_a, dtype=complex)

        v = self.get_initial_state(section_b=False)
        A[:, 0] = v
        purities_TLS[0], purities_bosons[0] = 1, 1  # Initial state is pure
        for i in range(1, N_steps_a):
            v = self.evolve_dt(v)
            A[:, i] = v
            purities_TLS[i], purities_bosons[i] = self.get_purities(v)

        A = self._mat_to_viewing_order(A)
        self.show_occupations(A, dt)
        self.show_power_in_g(A, dt)
        self.show_purities(purities_TLS, purities_bosons, dt)
        return A, purities_TLS, purities_bosons

    def simulate_cat_states(self):
        N_steps_b = 4000
        dt = 1e-9
        self.wa = 2*pi*5e9
        self._init_H_U(dt)

        A = np.zeros((1 + 2*self.N_bstates, N_steps_b), dtype=complex)
        purities_TLS = np.zeros(N_steps_b, dtype=complex)
        purities_bosons = np.zeros(N_steps_b, dtype=complex)

        v = self.get_initial_state(section_b=True)
        A[:, 0] = v
        purities_TLS[0], purities_bosons[0] = 1, 1  # Initial state is pure
        for i in range(1, N_steps_b):
            v = self.evolve_dt(v)
            A[:, i] = v
            purities_TLS[i], purities_bosons[i] = self.get_purities(v)

        A = self._mat_to_viewing_order(A)
        self.show_occupations(A, dt)
        self.show_power_in_g(A, dt)
        self.show_purities(purities_TLS, purities_bosons, dt)
        return A


if __name__ == "__main__":
    sim = JaynesCummingSimulation()
    # sim.simulate_revival()
    sim.simulate_cat_states()
    # plt.show()

# np.set_printoptions(threshold=sys.maxsize, linewidth=sys.maxsize)
